---
layout: post
title: Projekt 2032er startet
image: /assets/img/blog/projekt-2032er-startet.jpg
description: >
  Erster Artikel über das Projekt *2032er* mit kurzer Beschreibung, Anfangsideen und Aussichten. Die *2032er* Platte ist universell einsetzbar, erweitere sie nach belieben oder baue einfach selbst deine Platte oder denke dir weiteres Zubehör oder Erweiterungsmöglichkeiten aus. Denn alles ist Open Source!
---

Die Idee, eine universelle Platte nach Industriestandard zu bauen, ist geboren.
{:.lead}

[In Kürze, um was es geht](#in-kürze-um-was-es-geht), [Was kann man mit einer *2032er* alles machen?](#was-kann-man-mit-einer-2032er-alles-machen), [Wie alles anfing und wie es weitergeht](#wie-alles-anfing-und-wie-es-weitergeht), Lizenz: [CC-BY-SA 4.0](#cc-by-sa-40)

- Table of Contents
{:toc .large-only}

## In Kürze, um was es geht
Es geht darum einer Platte, ob als Tisch, Regal oder was auch immer, eine weitere Dimension zu geben. Bisher konnte man auf einer 2 dimensionalen Fläche etwas darauf legen, aber was ist mit der 3. Dimension? Etwas aufrecht zu fixieren, die Höhe zu nutzen oder überhaupt mehr Möglichkeiten an Flexibilität und Eigenkreativität beim selbst Basteln zu erhalten hat bisher gefehlt.

Um das zu ermöglichen ist die *2032er* Platte entstanden. Sie besitzt ein Lochraster mit der alle möglichen Aufbauten, Unterbauten, Oberflächen fixiert oder Platten verbunden werden können. Sie gibt es im ersten Anlauf in zwei Varianten: einer normalen Tischgröße von ca. 120 x 70 cm und einer schmalen Platte von ca. 120 x 30 cm. Diese können frei in der Fläche und in der Höhe kombiniert werden.

## Was kann man mit einer *2032er* alles machen?
Zuerst ist die *2032er* in ihrer Basis eine einfache Platte, die es - wie zuvor erwähnt - in zwei Varianten gibt. Die Einsatzbereiche der *2032er* sind vielfältig:

* Werkbank
* Schreibtisch
* Marktstand

um anfangs nur drei zu nennen.

In den folgenden Blogbeiträgen werde ich noch tiefer darauf eingehen und in Zukunft werden auch durch Deine Hilfe noch viele weitere Einsatzbereiche entstehen. Deiner Phantasie ist da keiner Grenze gesetzt.

## Wie alles anfing und wie es weitergeht
Wie alles anfing und woher die Inpsiration kam, werde ich Dir noch erzählen. Auf jeden Fall wäre noch wichtig zu erwähnen, dass das Projekt komplett Open-Source ist. Ob es die Platte selbst ist oder die Fotos oder die Anleitungen oder dieser Blog. Fühle Dich frei alles zu verwenden, abzuändern, zu erweitern oder einfach nur zu teilen. Damit uns allen das Projekt als freies Projekt erhalten bleibt, versuche ich alles gut und genau zu dokumentieren, damit keiner im Nachhinein irgendein Patent darüber legen kann.

#### CC-BY-SA 4.0

![CC-BY-SA 4.0](/assets/img/blog/cc-by-sa-40.png)

> Dieses Werk ist lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).

Was es mit der Lizenz auf sich hat und warum überhaupt Open Source, darauf werde ich auch noch weiter eingehen.

## Bleiben wir in Kontakt und sei gespannt

